module Main (main) where

import Control.Monad (when)
import Data.Bits (shiftR, testBit, (.&.))
import Data.Foldable (for_)
import Data.Int (Int64)
import System.IO (hPutStrLn, stderr)

type DecoderL = IO
type Offset = Int64

data Location = Mem Offset | Imm Int64
  deriving (Show)

data Instruction
  = Mov Location Offset
  | Add Location Location Offset
  | Neg Location Offset
  deriving (Show)

mask16 :: Int64
mask16 = 2 ^ (16 :: Int) - 1

throw :: String -> DecoderL a
throw = fail

logWarn :: String -> DecoderL ()
logWarn = hPutStrLn stderr

readInstruction ::
  [Int64] -> DecoderL (Maybe (Instruction, [Int64]))
readInstruction [] = pure Nothing
readInstruction (w : ws) = do
  let off1 = w .&. mask16
  let off2 = (w `shiftR` 16) .&. mask16
  let offDst = (w `shiftR` 32) .&. mask16
  let isImm1 = w `testBit` 48 -- src1
  let isImm2 = w `testBit` 49 -- src2
  let typBits = (w `shiftR` 49) .&. (2 ^ (14 :: Int) - 1)
  case typBits of
    0 -> do
      when (isImm1 || off2 /= 0) $ do
        logWarn "suspicious mov: second argument isn't zeroed out"
      (arg, ws') <- readLocation isImm1 off1 ws
      pure (Just (Mov arg offDst, ws'))
    1 -> do
      (arg1, ws') <- readLocation isImm1 off1 ws
      (arg2, ws'') <- readLocation isImm2 off2 ws'
      pure (Just (Add arg1 arg2 offDst, ws''))
    2 -> do
      when (isImm1 || off2 /= 0) $ do
        logWarn "suspicious neg: second argument isn't zeroed out"
      (arg, ws') <- readLocation isImm1 off1 ws
      pure (Just (Neg arg offDst, ws'))
    _ -> throw ("invalid type code: " <> show typBits)

readAllInstructions :: [Int64] -> DecoderL [Instruction]
readAllInstructions = unfoldrM readInstruction

unfoldrM :: Monad m => (b -> m (Maybe (a, b))) -> b -> m [a]
unfoldrM f seed = do
  output <- f seed
  case output of
    Nothing -> pure []
    Just (item, seed') -> (item :) <$> unfoldrM f seed'

readLocation :: Bool -> Int64 -> [Int64] -> DecoderL (Location, [Int64])
readLocation isImm offset ws
  | isImm = case ws of
      (w : ws') -> pure (Imm w, ws')
      [] -> throw "not enough words to read an immediate"
  | otherwise = pure (Mem offset, ws)

main :: IO ()
main = do
  ws <- map read . words <$> getContents
  instructions <- readAllInstructions ws
  for_ instructions $ \i ->
    print i
